## POC Guide

Welcome to this virtual PoC running in Cumulus in the Cloud. This documentation serves as the guide for the PoC in this virtual environment.

The simulation can be reached in the AIR (CITC) [here](https://air.cumulusnetworks.com/334fee23-9b3c-4aa3-99ce-221cd861b7f7/Simulation) where you are probably reading this. The automation for this PoC can be found in the Gitlab [repository](https://gitlab.com/nvidia-networking/systems-engineering/poc/fineco) for this project. 

Access is given individually, but when reading this you probably already have access. When this is not the case, ask your Nvidia contact person for this PoC. Any other credentials have been distributed in the team as well.

The virtual environment consists of multiple Cumulus Linux switches and Servers (Ubuntu 18.04) that are connected according to the diagram that can be viewed in this simulation GUI ("view topology" button). 

The next pages in this guide will walk you through the simulation, it's architecture, automation and NetQ. This guide focusses on the specific details for this PoC. Documentation for the configuration and all other details can be found through the links on the left side. In addition you will find PDFs of books that are released by Nvidia in the git repository. 

<!-- AIR:page -->
## Accessing and using the simulation

This simulation can be accessed either through this GUI or through SSH directly. Accessing through the GUI should be intuative (for any suggestions, please let us know) and pretty self-explanatory.

In the GUI node list a switch or server can be selected to open it's console. Here all normal actions can be done as if it would be a regular SSH session. You can open the advanced view on the left side to see the OOB server console and specific services settings.

In the services settings you will find the SSH details to directly ssh into the OOB server. Only key authentication is allowed, which means you have to add your ssh key to the OOB server (/home/cumulus/.ssh/authorized_keys). If using OpenSSH, a config alias would look like as follows:
```
Host poc-name
  Hostname air.cumulusnetworks.com
  Port 12345
  Protocol 2
  User cumulus
  IdentityFile ~/.ssh/xxx
  IdentitiesOnly yes
```

```
:~$ ssh poc-name
Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 4.15.0-88-generic x86_64)
                                                 _
      _______   x x x                           | |
 ._  <_______~ x X x   ___ _   _ _ __ ___  _   _| |_   _ ___
(' \  ,' || `,        / __| | | | '_ ` _ \| | | | | | | / __|
 `._:^   ||   :>     | (__| |_| | | | | | | |_| | | |_| \__ \
     ^T~~~~~~T'       \___|\__,_|_| |_| |_|\__,_|_|\__,_|___/
     ~"     ~"


############################################################################
#
#         Out Of Band Management Server (oob-mgmt-server)
#
############################################################################
Last login: Tue Mar 31 18:02:38 2020 from fd01:1:1:ffff::1
cumulus@oob-mgmt-server:~$
```

### Using the simulation

While the GUI allows opening indivdual consoles, it would be advisable to use the OOB server console as a jump host. This aligns with direct SSH access, since you ssh into the OOB server and jump to any node in the network. Also any automation would be run from there and it has internal ssh keys in place as well. This documentation will assume using the OOB server as jump host.

When logged in, you can directly ssh to any node in the diagram without credentials:
```
cumulus@oob-mgmt-server:~$ ssh leaf01
Linux leaf01 4.19.0-cl-1-amd64 #1 SMP Cumulus 4.19.94-1+cl4u3 (2020-03-05) x86_64

Last login: Tue Mar 31 18:22:14 2020 from 192.168.200.1
cumulus@leaf01:mgmt:~$ net show interface
State  Name            Spd  MTU    Mode           LLDP                     Summary
-----  --------------  ---  -----  -------------  -----------------------  ---------------------------
UP     lo              N/A  65536  Loopback                                IP: 127.0.0.1/8
       lo                                                                  IP: 10.10.10.1/32
       lo                                                                  IP: 10.10.100.1/32
       lo                                                                  IP: ::1/128
UP     eth0            1G   1500   Mgmt           oob-mgmt-switch (swp10)  Master: mgmt(UP)
       eth0                                                                IP: 192.168.200.11/24(DHCP)
UP     swp1            1G   9000   BondMember     server01 (eth1)          Master: bond1(UP)
UP     swp2            1G   9000   BondMember     server02 (eth1)          Master: bond2(UP)
UP     swp3            1G   9000   BondMember     server03 (eth1)          Master: bond3(UP)
UP     swp49           1G   9216   BondMember     leaf02 (swp49)           Master: peerlink(UP)
UP     swp50           1G   9216   BondMember     leaf02 (swp50)           Master: peerlink(UP)
UP     swp51           1G   9216   Default        spine01 (swp1)
UP     swp52           1G   9216   Default        spine02 (swp1)
UP     swp53           1G   9216   Default        spine03 (swp1)
```

On the devices you can look at the configuration, routing etc. Keep in mind that any changes that are made will be overwritten the next time the Ansible playbook will be run on the device.

While you can log in directly to the servers, they have a specific configuration to accommodate for a multi-tenant setup. In general, one should not have to login to the server directly. On each server there is a VM-like setup so that each vlan has one IP/MAC available. Multiple vlans are member of a VRF/L3VNI like in the following example diagram. The complete tenant list can be found in the [tenant inventory](https://gitlab.com/nvidia-networking/systems-engineering/poc/fineco/inventory/-/blob/master/group_vars/all/tenants.yml)

![PoC Topology](https://gitlab.com/nvidia-networking/systems-engineering/poc/fineco/documentation/-/raw/master/diagrams/tenants.png)

This setup allows for the following traffic flows:
- server01,vm1 - server04,vm49: L2-VxLAN
- server01,vm1 - server04,vm50: L3-VxLAN

While the VMs are actually namespaces on a Linux host, they can be used to send application traffic at a later stage such as multicast streams to test flows. At this time, each "VM" has an ip-address and can ping other hosts in the network (within the L3 tenancy restrictions). The available VMs can be found in the [VMs inventory file](https://gitlab.com/nvidia-networking/systems-engineering/poc/fineco/inventory/-/blob/master/group_vars/all/vms.yml).

To ping hosts in the network, login to a vm as follows:

```
cumulus@oob-mgmt-server:~$ ssh vm01
Welcome to Ubuntu 18.04.5 LTS (GNU/Linux 4.19.32-041932-generic x86_64)

cumulus@server01:~vm01$
```

Find an ip of a destination VM:
```
cumulus@server04:~vm50$ ip addr sh
1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
33: vm50lvm@if34: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc noqueue state UP group default qlen 1000
    link/ether 44:38:39:ff:00:60 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 192.168.1.32/24 brd 192.168.1.255 scope global vm50lvm
       valid_lft forever preferred_lft forever
    inet6 fe80::4638:39ff:feff:60/64 scope link
       valid_lft forever preferred_lft forever
35: vm50mvm@if36: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9000 qdisc noqueue state UP group default qlen 1000
    link/ether 44:38:39:bb:00:60 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 192.168.201.60/22 brd 192.168.203.255 scope global vm50mvm
       valid_lft forever preferred_lft forever
    inet6 fe80::4638:39ff:febb:60/64 scope link
       valid_lft forever preferred_lft forever
```

Use the IP address of the `vmXXlvm` interface and ping this from the vm:


```
cumulus@server01:~vm01$ ping 192.168.1.32
PING 192.168.1.32 (192.168.1.32) 56(84) bytes of data.
64 bytes from 192.168.1.32: icmp_seq=1 ttl=62 time=8.68 ms
64 bytes from 192.168.1.32: icmp_seq=2 ttl=62 time=7.84 ms
64 bytes from 192.168.1.32: icmp_seq=3 ttl=62 time=6.00 ms
64 bytes from 192.168.1.32: icmp_seq=4 ttl=62 time=5.39 ms
64 bytes from 192.168.1.32: icmp_seq=5 ttl=62 time=6.76 ms
--- 192.168.1.32 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4006ms
rtt min/avg/max/mdev = 5.390/6.937/8.682/1.197 ms
```

<!-- AIR:page -->
## Architecture

The architecture for this topology is according to the best practices that are being used when deploying Nvidia networks. It is a configuration with BGP EVPN-VxLAN. Earlier this year we added two new features, [EVPN-PIM](https://cumulusnetworks.com/blog/evpn-pim-part-one/) and EVPN-Multihoming that are being used as well. The best resources for these topics are the books in the aforementioned resource folder.

The routed underlay in the environment is all done with eBGP unnumbered. This means that on each point-to-point link an eBGP session has been configured. This can be seen on one of the leaf switches:
```
cumulus@leaf01:mgmt:~$ net show bgp sum
show bgp ipv4 unicast summary
=============================
BGP router identifier 10.10.10.1, local AS number 4200001011 vrf-id 0
BGP table version 268
RIB entries 39, using 7488 bytes of memory
Peers 3, using 64 KiB of memory
Peer groups 1, using 64 bytes of memory

Neighbor        V         AS   MsgRcvd   MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
spine01(swp51)  4 4200001021     88627     90008        0    0    0 00:13:18           46
spine02(swp52)  4 4200001021     87231     90875        0    0    0 00:13:18           46
spine03(swp53)  4 4200001021     85376     86940        0    0    0 00:13:18           46

Total number of neighbors 3


show bgp ipv6 unicast summary
=============================
% No BGP neighbors found


show bgp l2vpn evpn summary
===========================
BGP router identifier 10.10.10.1, local AS number 4200001011 vrf-id 0
BGP table version 0
RIB entries 499, using 94 KiB of memory
Peers 3, using 64 KiB of memory
Peer groups 1, using 64 bytes of memory

Neighbor        V         AS   MsgRcvd   MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
spine01(swp51)  4 4200001021     88627     90008        0    0    0 00:13:18         2593
spine02(swp52)  4 4200001021     87231     90875        0    0    0 00:13:18         2593
spine03(swp53)  4 4200001021     85376     86940        0    0    0 00:13:18         2593

Total number of neighbors 3
```

Each BGP session carrier the IPv4 and EVPN address family. The first is used to announce the loopback addresses which are the next-hop addresses in the EVPN overlay. As can be seen in the following output, each non-local address has three IPv6 (LLA, bgp unnumbered) next-hops. With ECMP traffic is being balanced over these three next-hops.
```
cumulus@leaf01:mgmt:~$ net show route
show ip route
=============
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR, f - OpenFabric,
       > - selected route, * - FIB route, q - queued route, r - rejected route

C>* 10.10.10.1/32 is directly connected, lo, 2d10h01m
B>* 10.10.10.2/32 [20/0] via fe80::4638:39ff:fe00:2f, swp51, weight 1, 00:15:22
  *                      via fe80::4638:39ff:fe00:37, swp52, weight 1, 00:15:22
  *                      via fe80::4638:39ff:fe00:3f, swp53, weight 1, 00:15:22
B>* 10.10.10.3/32 [20/0] via fe80::4638:39ff:fe00:2f, swp51, weight 1, 00:15:22
  *                      via fe80::4638:39ff:fe00:37, swp52, weight 1, 00:15:22
  *                      via fe80::4638:39ff:fe00:3f, swp53, weight 1, 00:15:22
B>* 10.10.10.4/32 [20/0] via fe80::4638:39ff:fe00:2f, swp51, weight 1, 00:15:22
  *                      via fe80::4638:39ff:fe00:37, swp52, weight 1, 00:15:22
  *                      via fe80::4638:39ff:fe00:3f, swp53, weight 1, 00:15:22

<output omitted>
```
 
In the environment there are three tenants configured. Each has it's own VRF/L3VNI attached configured. In the infrastructure a range of VLANs is linked to a VRF such that traffic between those vlans is contained to that specific tenant. A subset of the VLANs has been streched between the two sites.

In this tenant the IPs of the aforementioned VMs are learned, which makes all VMs reachable within that specific tenant. In the following output can be seen that the next-hop addresses are the loopback addresses announced in the underlay.
```
cumulus@leaf01:mgmt:~$ net show route vrf tenant1
show ip route vrf tenant1
==========================
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR, f - OpenFabric,
       > - selected route, * - FIB route, q - queued route, r - rejected route


VRF tenant1:
K>* 0.0.0.0/0 [255/8192] unreachable (ICMP unreachable), 2d10h06m
C>* 10.10.11.1/32 is directly connected, tenant1, 2d10h06m
B>* 10.10.11.2/32 [20/0] via 10.10.10.2, vlan4001 onlink, weight 1, 00:21:00
B>* 10.10.11.3/32 [20/0] via 10.10.10.3, vlan4001 onlink, weight 1, 00:21:00
B>* 10.10.11.4/32 [20/0] via 10.10.10.4, vlan4001 onlink, weight 1, 00:21:00
B>* 10.10.31.1/32 [20/0] via 10.10.30.1, vlan4001 onlink, weight 1, 00:21:00
B>* 10.10.31.2/32 [20/0] via 10.10.30.2, vlan4001 onlink, weight 1, 00:21:00
B>* 10.10.34.1/32 [20/0] via 10.10.30.1, vlan4001 onlink, weight 1, 00:21:00
B>* 10.10.34.2/32 [20/0] via 10.10.30.2, vlan4001 onlink, weight 1, 00:21:00

<output omitted>
```

Each server is connected with LACP to the leaf (ToR) switches. From a server perspective the two connected switches are the same switch. This is accomplished with EVPN-Multihoming. While the configuration for EVPN-MH is simple, the underlying technology can be complex. The best resource for this is the documentation or the upcoming [webinar](https://cumulusnetworks.com/learn/open-networking-webinars/evpn-multihoming/).

This production environment for this PoC will be based on VMware hosts. Vmware ESXi does have the possibility to use LACP or an active/passive configuration. This depends on the hypervisor/virtualisation design, but changing between the two configurations is a simple configuration.

Each bond for the connected server is a vlan trunk:
```
cumulus@leaf01:mgmt:~$ net show interface bond1001
    Name      MAC                Speed  MTU   Mode
--  --------  -----------------  -----  ----  -------
UP  bond1001  44:38:39:00:00:47  1G     9000  802.3ad

Bond Details
------------------  --------
Bond Mode:          802.3ad
Load Balancing:     layer3+4
Minimum Links:      1
LACP Sys Priority:
LACP Rate:          1
LACP Bypass:        Inactive

All VLANs on L2 Port
--------------------
1,1000-1004,2000-2004,3000-3004
```

The mac addresses from the VMs on the servers are learned in each of the VLANs:
```
cumulus@leaf01:mgmt:~$ net show bridge mac vlan 1001 | grep bond1001
1001  bridge  bond1001   44:38:39:ff:00:12              static                   00:02:30
```

This infrastructue consists of two datacenters with dark fiber connecting the two DCs. From a configuration perspective this means that these links are configured the same as any other leaf/spine link. While a DCI there are additional configuration possibilities (e.g filtering) for a DCI use-case, this is mostly applicable to larger scale environments.

<!-- AIR:page -->
## Automation

One of the key aspects of Cumulus Linux is the possibility to create a fully automated network infrastructure. Since Cumulus Linux is a standard Linux operating system, this is mostly done with DevOPS tools. Ansible is the most used automation tool for network environments today.

Beginning of this year the playbooks that are used in our professional services engagements have been [open-sourced](https://cumulusnetworks.com/blog/production-ready-automation/) ("Golden Turtle"). This infrastructure has been completely automated with playbooks that are based on the Golden Turtle playbooks. These are available in the git project: 

 * [Inventory](https://gitlab.com/nvidia-networking/systems-engineering/poc/fineco/inventory)
 * [Playbooks](https://gitlab.com/nvidia-networking/systems-engineering/poc/fineco/automation)

These playbooks can be used to change and update the environment. Typically it would only be necessary to update the variables in the inventory repository. E.g to add a vlan to the environment, the [tenant inventory](https://gitlab.com/nvidia-networking/systems-engineering/poc/fineco/inventory/-/blob/master/group_vars/all/tenants.yml) should be updated.

When variables are updated in the git repository, a `git pull` in the clone on the OOB server should be done. After which the playbook can be rolled out on the environment:
```
cumulus@oob-mgmt-server:~/fineco/automation$ ansible-playbook playbooks/deploy.yaml -l leaf,border --tags update

PLAY [oob-mgmt-server] **********************************************************************************
skipping: no hosts matched

PLAY [spine] ********************************************************************************************
skipping: no hosts matched

PLAY [leaf] *********************************************************************************************

TASK [Gathering Facts] **********************************************************************************
Saturday 17 October 2020  18:26:44 +0000 (0:00:00.072)       0:00:00.072 ******
ok: [leaf04]
ok: [leaf01]
ok: [leaf03]
ok: [leaf02]
ok: [leaf08]
ok: [leaf05]
ok: [leaf06]
ok: [leaf07]
```

For updating and changing the configuration through automation, it would be best to collaborate with the Nvidia pre-sales team.

<!-- AIR:page -->
## NetQ

Cumulus NetQ is the Streaming Telemetry solution from Nvidia when using Cumulus Linux. This software will gather and analyze data from the network to do faster and easier troubleshooting. More information can be found in the documentation.
